<?php


$date = date('Y-m-d H:i:s');
$enregistrement = $date . "\n";
$enregistrement .= "Nom : " . $_POST['nom'] . "\n";
$enregistrement .= "Prénom : " . $_POST['prenom'] . "\n";
$enregistrement .= "Adresse : " . $_POST['adresse'] . "\n";
$enregistrement .= "Ville : " . $_POST['ville'] . "\n";
$enregistrement .= "Code Postal : " . $_POST['code_postal'] . "\n";
$enregistrement .= "Numéro de Téléphone : " . $_POST['telephone'] . "\n";
$enregistrement .= "Email : " . $_POST['email'] . "\n";
$enregistrement .= "---------------------------\n";

$file = 'enregistrements.csv';

if (!file_exists($file)) {
    touch($file);
}

file_put_contents($file, $enregistrement, FILE_APPEND | LOCK_EX);

header("Location:enregistrement.php");
exit();
?>