<?php
$file = 'enregistrements.csv';

if (file_exists($file)) {
    $enregistrements = file_get_contents($file); // recupere contenu du fichier

    $enregistrements = explode("---------------------------\n", $enregistrements);

    echo '<table border="1">';
    echo '<tr><th>Date et Heure</th><th>Nom</th><th>Prénom</th><th>Adresse</th><th>Ville</th><th>Code Postal</th><th>Numéro de Téléphone</th><th>Email</th></tr>';

    foreach ($enregistrements as $enregistrement) {
        $donnees = explode("\n", $enregistrement);
        echo '<tr>';
        foreach ($donnees as $donnee) {
            echo '<td>' . $donnee . '</td>';
        }
        echo '</tr>';
    }

    echo '</table>';
} else {
    echo "Aucun enregistrement trouvé.";
}
?>