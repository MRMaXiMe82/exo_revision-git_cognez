// Index recettes

let recettes = ["falafel", "fricasse", "tiramisu"];

// Récupération du mot recherché

let search = document.getElementById("search");

search.addEventListener("keydown", function (event) {

    // Recherche = mot entré (id "search" de la barre de recherche), reponse = resultat
    let recherche = document.getElementById("search").value.toLowerCase();
    let reponse;

    //Detection de la touche entrée
    if (event.key === "Enter") {


        //Parcours le tableau recettes grace à for of
        for (let recette of recettes) {



            //On compare la recherche avec les différents éléments du tableau
            if (recette == recherche) {
                
                    //On redirige vers la page recherchée si trouvée
                  
                    document.location.assign(`recette.html#${recette}`);
                //On passe reponse à true pour dire que c'est bon
                  reponse = true;
                //On break pour casser la boucle for, au cas où on a plusieurs éléments dans le tableau après le resultat trouvé
                break;
            }
        }
        //On check enfin si la reponse est false pour renvoyer vers 404
        if (!reponse) {
            
        document.location.href = "404.html" ;
            
        }
    }
})