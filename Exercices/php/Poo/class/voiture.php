<?php
class Voiture{
    //Attributs
    public $energie;
    public $couleur;
    public $nbPorte;
    public $prix;
    public $marque;

    public function __construct($energie, $couleur, $nbPorte, $prix, $marque)
    {
        $this->energie = $energie;
        $this->couleur=$couleur;
        $this->nbPorte=$nbPorte;
        $this->prix=$prix;
        $this->marque=$marque;
    }
}