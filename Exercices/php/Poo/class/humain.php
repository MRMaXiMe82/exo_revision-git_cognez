<?php
class humain
{

    private $Firstname;
    private $Lastname;
    private $Email;
    private $Password;
    private $Adress;

    public function __construct($Firstname, $Lastname, $Email, $Password, $Adress)
    {
        $this->Firstname = $Firstname;
        $this->Lastname = $Lastname;
        $this->Email = $Email;
        $this->Password = $Password;
        $this->Adress = $Adress;
    }

    public function getFirstname(){return $this->Firstname;}

        
    
    public function setFirstname($NewFirstname)    { $this->Firstname = $NewFirstname;}

       
    


    public function getLastname()
    {
        return $this->Lastname;
    }
    public function setLasttname($NewLastname)
    {
        $this->Lastname = $NewLastname;
    }


    public function getEmail()
    {
        return $this->Email;
    }
    public function setEmail($NewEmail)
    {
        $this->Email = $NewEmail;
    }


    public function getPassword()
    {
        return $this->Password;
    }
    public function setPassword($NewPassword)
    {
        $this->Password = $NewPassword;
    }


    public function getAdress()
    {
        return $this->Adress;
    }
    public function setAdress($NewAdress)
    {
        $this->Adress = $NewAdress;
    }


    public function __tostring()
    {
        return "Mr ou Mme " . $this->Firstname ." ". $this->Lastname . " a pour email " . $this->Email . " et son mot de passe est " . $this->Password . " et il/elle habite au " . $this->Adress . ".";
    }

    public function speak($thing)
    {
        echo " Human say " . $thing . "!";
    }
}
