<?php
class animaux
{
    //attributs
    private $espece;
    private $age;
    private $lieu;
    private $image;
    private $alimentation;

    //fonction de construction
    public function __construct($espece, $age, $lieu, $image, $alimentation)
    {
        $this->espece = $espece;
        $this->setage($age);
        $this->lieu = $lieu;
        $this->image = $image;
        $this->alimentation = $alimentation;
    }

    // accesseur:get ou getteur
    public function getEspece()
    {
        return strtoupper($this->espece);
    }

    //mutateur:set ou setteur
    public function setEspece($nouveauNom)
    {

        $this->espece = $nouveauNom;
    }


    public function getlieu()
    {
        return $this->lieu;
    }
    public function setlieu($lieu)
    {
        $this->lieu = $lieu;
    }



    public function getage()
    {
        return " il a " . $this->age . " ans";
    }
    public function setage($age)
    {
        if ($age >= 0 && $age < 100) {
            $this->age = $age;
        }
    }



    public function getimage()
    {
        return '<img src="' . $this->image . '">';
    }
    public function setimage($newimage)
    {
        $this->image = $newimage;
    }



    public function getalimentation()
    {
        return $this->alimentation;
    }
    public function setalimentation($nouvelleAlimentation)
    {
        if (is_string($nouvelleAlimentation)) {
            $this->alimentation = $nouvelleAlimentation;
        }
    }

    public function __toString()
    {
        return "L'animal est un(e) " . $this->espece .$this->getage( ) .
         " il habite dans ". $this->lieu . " il/elle mange des " .
          $this->alimentation . $this->getimage() ."<br>";
    }
}
