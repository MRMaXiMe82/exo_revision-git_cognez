<?php

abstract class Animal {
    private $poids;
    private $hauteur;
    private $longueur;

    public function __construct($poids,$hauteur,$longueur)
    {
        $this->poids=$poids;
        $this->hauteur=$hauteur;
        $this->longueur=$longueur;
    }
public function getPoids(){
    return $this->poids;
}
public function setPoids($poids){
    $this->poids=$poids;
}


public function gethauteur(){
    return $this->hauteur;
}
public function sethauteur($hauteur){
    $this->hauteur=$hauteur;
}


public function getlongueur(){
    return $this->longueur;
}
public function setlongueur($longueur){
    $this->longueur=$longueur;
}



}