<?php

interface AnimalComportement{
    public function nager();
    public function apné();
    public function manger();
    public function marcher();
    public function boire();
    public function voler();

}