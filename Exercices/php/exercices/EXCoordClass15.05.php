<?php

class Profil
{

    private $fName;
    private $lName;
    private $adress;
    private $ville;
    private $codePost;
    private $tel;
    private $email;

    public function __construct($fName, $lName, $adress, $ville, $codePost, $tel, $email)
    {
        $this->fName = $fName;
        $this->lName = $lName;
        $this->adress = $adress;
        $this->ville = $ville;
        $this->codePost = $codePost;
        $this->tel = $tel;
        $this->email = $email;
    }

    public function getfName():string{return $this->fName;}
    public function setfName($fName){$this->fName = $fName;}

    public function getlName():string{return $this->lName;}
    public function setlName($lName){$this->lName = $lName;}

    public function getadress():string{return $this->adress;}
    public function setadress($adress){$this->adress = $adress;}

    public function getville():string{return $this->ville;}
    public function setville($ville){$this->ville = $ville;}

    public function getcodePost():int{return $this->codePost;}
    public function setcodePost($codePost){$this->codePost = $codePost;}

    public function gettel():int{return $this->tel;}
    public function settel($tel){$this->tel = $tel;}

    public function getemail():string{return $this->email;}
    public function setemail($email){$this->email = $email;}

    public function dateHeure($user){
        echo "date/heure" . $user ;
    }
public function __toString()
{
    return $this->fName." ".
    $this->lName." ".
    $this->adress." ".
    $this->ville." ".
    $this->codePost." ".
    $this->tel." ".
    $this->email;
}

}
