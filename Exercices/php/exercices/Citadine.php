<?php

class citadine extends Vehicule
{
    private $longueur;

    public function __construct($longueur, $couleur, $Nbportes)
    {
        parent::__construct($Nbportes, $couleur);
        $this->longueur = $longueur;
    }

    public function getlongueur()
    {
        return $this->longueur;
    }
    public function setlongueur($longueur)
    {
        $this->longueur = $longueur;
    }

    public function __toString()
    {
        return parent::__toString() . "Cette citadine mesure " . $this->longueur . "mètre" . "<br>";
    }

    public function revision()
    {
        echo " La citadine fait sa révision <br>";
    }
}
