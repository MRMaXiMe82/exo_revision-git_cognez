<?php

class Human
{
    private $fname;

    private $lname;

    private $mail;

    private $password;

    private $adress;


    public function __construct($fname, $lname, $mail, $password, $adress)
    {
        $this->fname = $fname;
        $this->lname = $lname;
        $this->mail = $mail;
        $this->password = $password;
        $this->adress = $adress;
    }

    public function getFname()
    {
        return $this->fname;
    }
    public function setFname($fname)
    {
        $this->fname = $fname;
    }


    public function getLname()
    {
        return $this->lname;
    }
    public function setLname($lname)
    {
        $this->lname = $lname;
    }


    public function getMail()
    {
        return $this->mail;
    }
    public function setMail($mail)
    {
        $this->mail = $mail;
    }


    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($password)
    {
        $this->password = $password;
    }


    public function getAdress()
    {
        return $this->adress;
    }
    public function setAdress($adress)
    {
        $this->adress = $adress;
    }


    public function speak($thing)
    {
        echo "Human say: " . $thing;
    }

    
    public function __toString()
    {
        return
            "You're name is" . $this->lname . " " . $this->fname . "<br>" .
            "Email: " . $this->mail . "<br>" .
            "Password: " . $this->password . "<br>" .
            "You're adress is: " . $this->adress;
    }
}
