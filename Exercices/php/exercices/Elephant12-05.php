<?php
require_once 'ClassAnimal12-05.php';
require_once 'AnimalComportement.php';
class Elephant extends Animal {

    private $nbDéfences;

    public function __construct($nbDéfences,$poids,$hauteur,$longueur)
    {
        parent::__construct($poids,$hauteur,$longueur);
        $this->nbDéfences=$nbDéfences;
    }

    public function getnbDéfences()
    {
        return $this->nbDéfences;
    }
    public function setnbDéfences($nbDéfences)
    {
        $this->nbDéfences = $nbDéfences;
    }

    public function marcher(){
        echo "L'éléphant marche";
    }
    public function nager(){
        echo "L'éléphant nage";
    }
    public function apné(){
        echo "L'éléphant ne fait pas d'apné";
    }
    public function manger(){
        echo "L'éléphant mange des feuilles";
    }



}