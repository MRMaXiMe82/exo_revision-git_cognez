<?php

abstract class Vehicule
{
    //Attributs
    private $Nbportes;

    private $couleur;





    public function __construct($Nbportes, $couleur)
    {
        $this->Nbportes = $Nbportes;
        $this->couleur = $couleur;
    }
    public function getNbportes()
    {
        return $this->Nbportes;
    }
    public function setNbportes($Nbportes)
    {
        $this->Nbportes = $Nbportes;
    }


    public function getCouleur()
    {
        return $this->couleur;
    }
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;
    }



    public function __toString()
    {
        return "Vous avez un vehicule avec " . $this->Nbportes . " portes<br>" .
            "La couleur est " . $this->couleur . "<br>";
    }


    public function demarrer()
    {
        echo "vroum vroum!";
    }

    abstract public function revision();
   
}



