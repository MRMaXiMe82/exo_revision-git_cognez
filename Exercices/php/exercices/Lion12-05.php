<?php
require_once 'ClassAnimal12-05.php';
require_once 'AnimalComportement.php';
class Lion extends Animal {

    private $tpsRepos;

    public function __construct($tpsRepos,$poids,$hauteur,$longueur)
    {
        parent::__construct($poids,$hauteur,$longueur);
        $this->tpsRepos=$tpsRepos;
    }

    public function gettpsRepos()
    {
        return $this->tpsRepos;
    }
    public function settpsRepos($tpsRepos)
    {
        $this->tpsRepos = $tpsRepos;
    }

    public function marcher(){ 
        echo "Le lion marche";
    }
    public function nager(){
        echo "Le lion nage";
    }
    public function apné(){
        echo "Le lion ne fait pas d'apné";
    }
    public function manger(){
        echo "Le lion mange des antiloppes";
    }



}