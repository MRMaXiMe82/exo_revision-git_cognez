<?php
class utilitaire extends Vehicule
{
    private $ptac;

    public function __construct($ptac, $couleur, $Nbportes)
    {
        parent::__construct($Nbportes, $couleur);
        $this->ptac= $ptac;
    }

    public function getptac()
    {
        return $this->ptac;
    }
    public function setptac($ptac)
    {
        $this->ptac = $ptac;
    }

    public function __toString()
    {
        return parent::__toString() . "Cet utilitaire peut charger au maximum " . $this->ptac . "kg" . "<br>";
    }
    public function revision()
    {
        echo " La utlitaire fait sa révision <br>";
    }
}
