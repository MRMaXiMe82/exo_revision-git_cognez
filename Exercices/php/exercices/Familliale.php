<?php

class familliale extends Vehicule
{
    private $Nbplaces;

    public function __construct($Nbplaces, $couleur, $Nbportes)
    {
        parent::__construct($Nbportes, $couleur);
        $this->Nbplaces = $Nbplaces;
    }

    public function getNbplaces()
    {
        return $this->Nbplaces;
    }
    public function setNbplaces($Nbplaces)
    {
        $this->Nbplaces = $Nbplaces;
    }


    public function __toString()
    {
        return parent::__toString() . " Cette familliale a " . $this->Nbplaces  . "<br>"." La familliale fait sa révision <br>";
    }

    public function revision()
    {
        echo " La familliale fait sa révision <br>";
    }
}