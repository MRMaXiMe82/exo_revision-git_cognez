<?php
require_once 'ClassAnimal12-05.php';
require_once 'AnimalComportement.php';
class Croco extends Animal {

    private $amplitudeGueule;

    public function __construct($amplitudeGueule,$poids,$hauteur,$longueur)
    {
        parent::__construct($poids,$hauteur,$longueur);
        $this->amplitudeGueule=$amplitudeGueule;
    }

    public function getamplitudeGueule()
    {
        return $this->amplitudeGueule;
    }
    public function setamplitudeGueule($amplitudeGueule)
    {
        $this->amplitudeGueule = $amplitudeGueule;
    }

    public function marcher(){
        echo "Le croco marche";
    }
    public function nager(){
        echo "Le croco nage";
    }
    public function apné(){
        echo "Le croco fait des apnés de compétition";
    }
    public function manger(){
        echo "Le croco mange des gnous";
    }



}