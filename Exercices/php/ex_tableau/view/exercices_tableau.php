<?php

echo "exercice 1 <br>";

//exo1 
// $tab = array();

// $tab[0] = 0;
// $tab[1] = 0;
// $tab[2] = 0;
// $tab[3] = 0;
// $tab[4] = 0;
// $tab[5] = 0;
// $tab[6] = 0;

// echo $tab[0] . "<br>";
// echo $tab[1] . "<br>";
// echo $tab[2] . "<br>";
// echo $tab[3] . "<br>";
// echo $tab[4] . "<br>";
// echo $tab[5] . "<br>";
// echo $tab[6] . "<br>";
$tab = [];

for ($i = 0; $i < 7; $i++) {

    $tab[$i] = 0;
    echo $tab[$i] . "<br>";
}

echo "<hr> exercice 2 <br>";

//exo2 
// $tab[0] = "a";
// $tab[1] = "e";
// $tab[2] = "i";
//...
$Tableau = ["a", "e", "i", "o", "u", "y"];


for ($i = 0; $i < count($Tableau); $i++) {
    echo $Tableau[$i] . "<br>";
}
echo "<hr> exercice 3 <br>";

//exo 3
$tab = array(10, 15, 20, 25, 30);
$sum = 0;

for ($i = 0; $i < count($tab); $i++) {
    $sum += $tab[$i];
}
echo "La somme des valeurs du tableau est : " . $sum . "<br>";
echo array_sum($tab);

echo "<hr> exercice 4 <br>";

//exo 4
$mois = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');

echo "<ol>";

foreach ($mois as $valeur) {
    echo "<li>" . $valeur . "</li>";
}

echo "</ol>";

echo "<hr> exercice 5 <br>";

//exo 5 

$tab = array(-99, -34, -22, -15, -10);

$max = $tab[0];
$min = $tab[0];
$max_pos = 0;
$min_pos = 0;


for ($i = 1; $i < count($tab); $i++) {

    if ($tab[$i] > $max) {

        $max = $tab[$i];
        $max_pos = $i;
    } elseif ($tab[$i] < $min) {

        $min = $tab[$i];
        $min_pos = $i;
    }
}
// ou bien : 
// $max = max($tab);
// $min = min($tab);
// $max_pos = array_search($max,$tab);
// $min_pos = array_search($min,$tab);



echo "La valeur maximale est : " . $max . " et sa position est : " . $max_pos . "<br>";
echo "La valeur minimale est : " . $min . " et sa position est : " . $min_pos . "<br>";

echo "<hr> exercice 6 <br>";

//exo 6
//un tableau est composé de clé et de valeur 

$anne = [
    "Janvier" => 31,
    "Fevrier" => 28,
    "Mars" => 31,
    "Avril" => 30,
    "Mai" => 31,
    "Juin" => 30,
    "Juillet" => 31,
    "Aout" => 31,
    "Septembre" => 30,
    "octobre" => 31,
    "novembre" => 30,
    "Decembre" => 31
];

echo '
    <table border=1>
        <tr>
          <th>Mois</th>
          <th> Nb Jours </th>
        </tr>
';

foreach ($anne as $mois => $jour) {

    echo  "<tr> 
                <td> " . $mois . '</td>';
    echo        "<td>" . $jour . "</td> 
            </tr>";
}

// utiliser un foreach avec des clés et des valeurs pour afficher chaque lignes


echo '</table>';


echo '<hr> Exercice 7 <br> ';

$tab = [-9, -100, 55, 99, 45, 3, 1, 254, -65];

asort($tab);

foreach ($tab as $value) {
    echo $value . '<br>';
}



echo '<hr> Exercice 8 <br> ';

$tab = [-9, -100, 55, 99, 45, 3, 1, 254, -65];

arsort($tab);

foreach ($tab as $value) {
    echo $value . '<br>';
}