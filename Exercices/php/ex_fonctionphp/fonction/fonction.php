<?php

// ex1

function hello()
{
    echo 'Bonjour' . '<br>';
}

hello();



// ex2 

function afficherNombres()
{
    for ($i = 1; $i <= 100; $i++) {
        echo $i . ',';
        if (($i == 10) || ($i == 20) || ($i == 30) || ($i == 40) || ($i == 50) || ($i == 60) || ($i == 70) || ($i == 80) || ($i == 90)) {
            echo '<br>';
        }
    }
}

afficherNombres();
echo '<br><br>';

// function afficherNombres() {
//     for ($i = 1; $i <= 100; $i++) {
//         echo $i . ' ';
//         if ($i % 10 == 0) {
//             echo '<br>';
//         }
//     }
// }

// afficherNombres();



// ex3



function sum($nombre1, $nombre2)
{
    $somme = $nombre1 + $nombre2;
    echo "La somme est: " . $somme . '<br>';
}
sum(22, 68);


// ex4
function longueurChaine($chaine)
{
    $length = strlen($chaine);
    return $length;
}
$str = "ceci est une chaine";
$length = longueurChaine($str);
echo "La chaîne $str contient $length caractères." . "<br>";

// function longueurChaine($chaine){
//     $length = strlen($chaine);
//     return $length;
// }
// $str = "ceci est un test ! ";
// $length = longueurChaine($str);
// echo "La chaîne $str contient $length caractères."."<br>";

// ex5
// str_replace($search, $replace, $subject);


function remplaceCaractere($str)
{
    $textRemplacer = str_replace(["e","E"], "_", $str);

    return ($textRemplacer);
}
$str = "ceci est une chaine de caractere ac des 'e' et des espaces.";

echo remplaceCaractere($str);

// ex6
$tabStr = ["TotO => 'taTA",
            "BOnjour => 'christOPhe!!"];

 function listeVal($tabStr){
    foreach($tabStr as $key => $value){
        echo strtoupper( $key ) . " : " .  strtolower($value)  . " (" . strlen($value) . " ) <br>";
        
    }


 }

//  $tabStr = ["TotO" => 'TATA',
//             "BOnjOur" => 'christOPhe !!'];

// function listVal($tableau){

//     foreach ($tableau as $key => $value) {
        
//         echo strtoupper( $key ) . " : " .  strtolower($value)  . " (" . strlen($value) . " ) <br>";       

//     }
    

// }

// listVal($tabStr);