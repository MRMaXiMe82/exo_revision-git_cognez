<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ex AnimauxTabMult ac imbrication</title>
</head>
<body>
    <h1>AnimauxTabMult ac imbrication</h1>

    <?php 
    $animaux = [
        "chat" =>
        [
            "longévité" => 15,
            "couleur"   => "rouge",
            "lieu"      => "goutière",

            "alimentation" => [
                "croquette",
                "souris",
                "vache"
            ],
            "image" => "https://i.pinimg.com/originals/c9/f2/3e/c9f23e212529f13f19bad5602d84b78b.jpg",
        ],
        "chien" =>
        [
            "longévité" => 20,
            "couleur"   => "blouge",
            "lieu"      => "panier",

            "alimentation" => [
                "chat",
                "paté",
                "gâteau"
            ],
            "image" => "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/555a3882-219a-4327-9064-315ca41d8a73/d4brqxz-9631a692-953b-4b49-8da4-885697542871.jpg/v1/fill/w_1600,h_1067,q_75,strp/light_violet_dog_by_devilkkw-d4brqxz.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwic3ViIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl0sIm9iaiI6W1t7InBhdGgiOiIvZi81NTVhMzg4Mi0yMTlhLTQzMjctOTA2NC0zMTVjYTQxZDhhNzMvZDRicnF4ei05NjMxYTY5Mi05NTNiLTRiNDktOGRhNC04ODU2OTc1NDI4NzEuanBnIiwid2lkdGgiOiI8PTE2MDAiLCJoZWlnaHQiOiI8PTEwNjcifV1dfQ.TtvjPyvmDrBJ4CmoNXpkgTqSAh5wud0jXnt3GC32bXM",
        ],
        "giraffe" =>
        [
            "longévité" => 45,
            "couleur"   => "rose",
            "lieu"      => "savane",

            "alimentation" => [
                "feuille",
                "herbe",
                "baobab"
            ],
            "image" => "https://i.pinimg.com/originals/de/a9/6c/dea96ce0a3baf050f14a75c7bf1de3ea.jpg",
        ],
        "zèbre" =>
        [
            "longévité" => 50,
            "couleur"   => "bleu & jaune",
            "lieu"      => "savane",

            "alimentation" => [
                "herbe",
                "feuille",
                "acacia"
            ],
            "image"        => "https://th.bing.com/th/id/OIP.ANKkep46TB22Y79q-vEpjQHaHX?pid=ImgDet&rs=1",
        ],

    ];
    ?>
    <h1>Les animaux</h1>

<table border="1">
    <tr>
        <th>Nom </th>
        <th>Longévité</th>
        <th> Couleur </th>
        <th>Lieu</th>
        <th>Alimentation</th>
        <th>Image</th>
    </tr>

    <!-- Code php avec un foreach -->

    <?php foreach ($animaux as $animal => $donnee) : ?>

        <tr>

            <td><?php echo $animal ?></td>

            <?php foreach ($donnee as $key => $value) : ?>

                <?php if ($key == "alimentation") :  ?>

                    <td>
                        <ul>
                     
                             <?php  foreach ($value as $aliment) : ?>
                           

                                <li><?php echo $aliment  ?></li>

                            <?php endforeach?>
                            
                            
                        </ul>
                    </td>
                    
                <?php elseif ($key == "image") : ?>

                        <td><img src=" <?php echo $value  ?> " width="200px" height="150px" ></td>

                    <?php  else : ?>

                    <td><?php echo $value ?></td>

                <?php  endif ?>
            <?php  endforeach ?>

        </tr>

    <?php endforeach ?>



</table>
</body>
</html>

 <?php ?> 