<?php

$listeDesPokemon = [

    'Pikachu' => [
        'Taille' => 0.4,
        'Poids' => 6,
    ],
    'Raichu' => [
        'Taille' => 0.8,
        'Poids' => 30,
    ],
    'Salameche' => [
        'Taille' => 1,
        'Poids' => 50,
    ],
    'Reptincel' => [
        'Taille' => 1.4,
        'Poids' => 60,
    ],
    'Dracaufeau' => [
        'Taille' => 2,
        'Poids' => 180,
    ],

];


?>

<h1>Pokemon</h1>
<ul>
    <?php foreach ($listeDesPokemon as $family => $value) { ?>

    <li><?php echo $family ?> : </li>


    <?php foreach ($value as $parents  => $nom) { ?>

        
            <ul>
                <li> <?php echo $parents ?> : <?php echo $nom ?></li>
            </ul>

        
    <?php
    }
}   ?>
</ul>



