// Create your variables here
// ==========================================
let numberOfSeasons = 8;
let numberOfEpisodes = 10;
let episodeTime = 50;
let commercialTime = 5;
let totalShowTime = (episodeTime + commercialTime) * numberOfSeasons * numberOfEpisodes;
 
// ==========================================

let paragraph = document.querySelector('#info');
paragraph.innerText = `${numberOfSeasons} saisons, ${numberOfEpisodes} épisodes par saison

Durée total de visionnage: ${ totalShowTime} minutes; `