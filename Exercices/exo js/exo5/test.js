// Create Object here
// =========================================
episode = {
    title: "Le dernier bar avant la fin du monde...",
    duration: 58,
    hasBeenWatched: true
};


// =========================================

document.querySelector('#episode-info').innerText = `Épisode: ${episode.title}
Durée: ${episode.duration} min
${episode.hasBeenWatched ? 'Déjà regardé' : 'Pas encore regardé'}`