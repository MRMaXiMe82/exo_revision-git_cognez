// Create variables here
// =========================================
let episodeTitle = "l'histoire du codage" ;
let episodeDuration = 52;
let hasBeenWatched = true;
// =========================================

document.querySelector('#episode-info').innerText = `Épisode: ${episodeTitle}
Durée: ${episodeDuration} min
${hasBeenWatched ? 'Déjà regardé' : 'Pas encore regardé'}`